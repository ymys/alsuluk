import { IsNotEmpty } from "class-validator";

export class JournalDTO {
    @IsNotEmpty()
    title:String;

    @IsNotEmpty()
    description:String;
    
    categories: Array<string>;
}