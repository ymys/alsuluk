import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { JournalSchema } from '../models/journal.schema';
import { JournalService } from './journal.service';
import { JournalController } from './journal.controller';
import { CategorySchema } from 'src/models/category.schema';


@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Journal', schema: JournalSchema }]),
    MongooseModule.forFeature([{ name: 'Category', schema: CategorySchema }]),
  ],
  providers: [JournalService],
  controllers: [JournalController]
})
export class JournalModule {}
