import { Body, Controller, Get, Post, UseGuards, Request, Delete, Param, Put, Response } from '@nestjs/common';
import { ACGuard, UseRoles } from 'nest-access-control';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { JournalService } from 'src/journal/journal.service';
import {JournalDTO} from './dto/journal.dto'

@Controller('journal')
export class JournalController {

    constructor(
        private journalService: JournalService,
    ) {}

    @Get('/')
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async get(@Response() res, @Request() req) {
        const journal = await this.journalService.get(req.user._id);
        res.status(200)
            .json({
                'statusCode': 200,
                'message': 'Data journal berhasil didapatkan', 
                'values' : journal
            });
    }

    @Post('/')
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async self(@Response() res, @Request() req , @Body() JournalDTO: JournalDTO) {
        let newJournal = {...JournalDTO, user: req.user._id}
        const journal = await this.journalService.store(newJournal);
        
        res.status(201)
            .json({
                'statusCode': 201,
                'message': 'Data journal berhasil disimpan', 
                'values' : journal
            });
    }

    @Put('/:id')
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async update(@Response() res, @Param('id') id, @Body() JournalDTO: JournalDTO) {
        const journal = await this.journalService.update(id, JournalDTO);
        
        res.status(200)
            .json({
                'statusCode': 200,
                'message': 'Data journal berhasil diupdate', 
                'values' : journal
            });
    }

    @Delete('/:id')
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async destroy(@Response() res, @Param('id') id) {
        const journal = await this.journalService.destroy(id);
        
        res.status(200)
            .json({
                'statusCode': 200,
                'message': 'Data journal berhasil dihapus', 
                'values' : journal
            });
    }

}
