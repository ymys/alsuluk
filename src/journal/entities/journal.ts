export class Journal extends Document {
    categories: Array<string>;
    user: string;
    title: string;
    description: string;
}