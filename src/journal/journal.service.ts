import { Injectable, HttpException, HttpStatus, NotFoundException } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Journal } from 'src/journal/entities/journal';
import {JournalDTO} from './dto/journal.dto'
import { Category } from 'src/categories/entities/categories';

@Injectable()
export class JournalService {

    constructor(
        @InjectModel('Journal') private journalModel: Model<Journal>,
        @InjectModel('Category') private categoryModel: Model<Category>
    ) {}

    async get(userId: String) {
        const journal = await this.journalModel.find({ user: userId } ).populate('categories');
        return journal;
    }

    async store(JournalDTO: JournalDTO) {
        let payload = JournalDTO;

        if(payload.categories && payload.categories.length) {
            let categories =
                await this.categoryModel
                .find({name: {$in: payload.categories}});

            if(categories.length){
                payload = {...payload, categories: categories.map( category => category._id)}
            } else {
                delete payload.categories
            }
        }

        let journal = await this.journalModel.create(payload);

        return journal;
    }

    async update(id, JournalDTO: JournalDTO) {
        let payload = JournalDTO;

        if(payload.categories && payload.categories.length) {
            let categories =
                await this.categoryModel
                .find({name: {$in: payload.categories}});

            if(categories.length){
                payload = {...payload, categories: categories.map( category => category._id)}
            } else {
                delete payload.categories
            }
        }

        const journal = await this.journalModel.findOneAndUpdate({_id: id}, payload);

        if (!journal) {
            throw new NotFoundException('journal not found');
        }

        return journal;
    }

    async destroy(id) {
        const journal = await this.journalModel.findOneAndDelete({_id: id});
        if (!journal) {
            throw new HttpException('journal not found', HttpStatus.BAD_REQUEST);
        }
        return journal;
    }
}
