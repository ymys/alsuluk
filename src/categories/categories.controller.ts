import { Body, Controller, Get, Post, UseGuards, Request, Delete, Param, Put, Response } from '@nestjs/common';
import { ACGuard, UseRoles } from 'nest-access-control';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { CategoriesService } from 'src/categories/categories.service';
import {SelfCategoriesDTO} from './dto/self-categories.dto'
import {DefaultCategoriesDTO} from './dto/default-categories.dto'

@Controller('categories')
export class CategoriesController {

    constructor(
        private categoryService: CategoriesService,
    ) {}

    @Get('/')
    @UseGuards(AuthGuard("jwt"), ACGuard)
    @UseRoles({
        resource:  'categories',
        action:  'read',
        possession:  'any',
    })
    async get(@Response() res, @Request() req) {
        const category = await this.categoryService.get(req.user._id);
        res.status(200)
            .json({
                'statusCode': 200,
                'message': 'Data category berhasil didapatkan', 
                'values' : category
            });
    }

    @Post('self')
    @UseRoles({
        resource:  'categoriesSelf',
        action:  'create',
        possession:  'any',
    })
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async self(@Response() res, @Request() req, @Body() SelfCategoriesDTO: SelfCategoriesDTO) {
        let newCategory = {...SelfCategoriesDTO, user: req.user._id}
        const category = await this.categoryService.createSelfCategory(newCategory);
        res.status(201)
            .json({
                'statusCode': 201,
                'message': 'Data category berhasil disimpan', 
                'values' : category
            });
    }

    @Post('default')
    @UseRoles({
        resource:  'categoriesDefault',
        action:  'create',
        possession:  'any',
    })
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async default(@Response() res, @Request() req , @Body() DefaultCategoriesDTO: DefaultCategoriesDTO) {
        let newCategory = {...DefaultCategoriesDTO, user: req.user._id}
        const category = await this.categoryService.createDefaultCategory(newCategory);
        res.status(201)
        .json({
            'statusCode': 201,
            'message': 'Data category berhasil disimpan', 
            'values' : category
        });
    }

    @Delete('/:id')
    @UseRoles({
        resource:  'category',
        action:  'delete',
        possession:  'any',
    })
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async destroy(@Response() res, @Param('id') id) {
        const category = await this.categoryService.destroy(id);
        res.status(200)
            .json({
                'statusCode': 200,
                'message': 'Data category berhasil dihapus', 
                'values' : category
            });
    }

    @Put('/:id')
    @UseRoles({
        resource:  'category',
        action:  'update',
        possession:  'any',
    })
    @UseGuards(AuthGuard("jwt"), ACGuard)
    async update(@Response() res, @Param('id') id, @Body() SelfCategoriesDTO: SelfCategoriesDTO) {
        const category = await this.categoryService.update(id, SelfCategoriesDTO);
        res.status(200)
            .json({
                'statusCode': 200,
                'message': 'Data category berhasil diupdate', 
                'values' : category
            });
    }
}
