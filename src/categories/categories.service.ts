import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Category } from 'src/categories/entities/categories';
import {SelfCategoriesDTO} from './dto/self-categories.dto'
import {DefaultCategoriesDTO} from './dto/default-categories.dto'

@Injectable()
export class CategoriesService {
    constructor(
        @InjectModel('Category') private categoryModel: Model<Category>,
    ) {}
    
    async createSelfCategory(SelfCategoriesDTO: SelfCategoriesDTO) {
        const payload = SelfCategoriesDTO;
        const category = await this.categoryModel.create(payload);
        return category;
    }

    async createDefaultCategory(DefaultCategoriesDTO: DefaultCategoriesDTO) {
        const payload = {...DefaultCategoriesDTO, is_default: true};
        const category = await this.categoryModel.create(payload);
        return category;
    }

    async get(userId: String) {
        const category = await this.categoryModel.find({ $or: [{ user: userId }, { is_default: true }] });
        return category;
    }

    async destroy(id) {
        const category = await this.categoryModel.findOneAndDelete({_id: id, is_default: false});
        if (!category) {
            throw new HttpException('category not found', HttpStatus.BAD_REQUEST);
        }
        return category;
    }

    async update(id, SelfCategoriesDTO: SelfCategoriesDTO) {
        const category = await this.categoryModel.findOneAndUpdate({_id: id, is_default: false}, SelfCategoriesDTO);
        if (!category) {
            throw new HttpException('category not found', HttpStatus.BAD_REQUEST);
        }
        return category;
    }
}
