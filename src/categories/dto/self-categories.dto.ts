import { IsNotEmpty } from "class-validator";

export class SelfCategoriesDTO {
    @IsNotEmpty()
    name:String;
}