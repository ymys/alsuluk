import { IsNotEmpty } from "class-validator";

export class DefaultCategoriesDTO {
    @IsNotEmpty()
    name:String;

    is_default: Boolean = true;
}