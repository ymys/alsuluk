import { Document } from 'mongoose'

export class Category extends Document {
    name: string;
    is_default: boolean;
}