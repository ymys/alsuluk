import { InjectModel } from "@nestjs/mongoose";
import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { Model } from 'mongoose';
import { User } from "src/user/entities/user";

@ValidatorConstraint({ name: 'unique', async: true })
export class UniqueValidator implements ValidatorConstraintInterface {
    constructor(
        @InjectModel('User') private userModel: Model<User>,
      ) {}
    
      async validate(value: any, args: ValidationArguments) {
        const filter = {};
        filter[args.property] = value;
        const user = await this.userModel.findOne(filter);
        return !user;
      }
    
      defaultMessage(args: ValidationArguments) {
        return "$(value) is already taken";
      }
}