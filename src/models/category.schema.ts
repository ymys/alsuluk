import * as mongoose from 'mongoose';

export const CategorySchema = new mongoose.Schema({
    user: {
      type: mongoose.Schema.Types.ObjectId, 
      ref: 'User'
    },
    name: {
      type:String,
      required:true
    },
    is_default: {
      type: Boolean,
      default: false
    },
});
