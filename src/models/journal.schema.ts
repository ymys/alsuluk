import * as mongoose from 'mongoose';

export const JournalSchema = new mongoose.Schema({
    categories: [
        {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Category'
        }
    ],
    user: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    title: {
      type:String,
      required:true
    },
    description: {
      type: String,
      required: true
    },
});
