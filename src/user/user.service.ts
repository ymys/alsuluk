import { Injectable,  Inject, forwardRef, UnauthorizedException, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/user/entities/user';
import { RegisterDTO } from '../auth/dto/register.dto';
import * as bcrypt from 'bcrypt';
import { LoginDTO } from 'src/auth/dto/login.dto';
import { Payload } from 'src/auth/entities/payload';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class UserService {
    constructor(
        @InjectModel('User') private userModel: Model<User>,
        @Inject(forwardRef(() => AuthService))
        private authService: AuthService,
    ) {}

    async create(RegisterDTO: RegisterDTO) {
        const { email } = RegisterDTO;
        const user = await this.userModel.findOne({ email });
        if (user) {
            throw new BadRequestException('user already exists');
        }
        const createdUser = new this.userModel(RegisterDTO);
        await createdUser.save();
        return this.sanitizeUser(createdUser);
    }

    async findByLogin(UserDTO: LoginDTO) {
        const { email, password } = UserDTO;
        const user = await this.userModel.findOne({ email });
        if (!user) {
            throw new NotFoundException('user doesnt exists');
        }
        if (await bcrypt.compare(password, user.password)) {
            let token = await this.authService.signPayload({email: user.email});
            let newUser = await this.userModel.findByIdAndUpdate(user._id, { access_token: token });
            return this.sanitizeUser(newUser)
        } else {
            throw new UnauthorizedException('invalid credential');
        }
    }

   // return user object without password
    sanitizeUser(user: User) {
        const sanitized = user.toObject();
        delete sanitized['password'];
        return sanitized;
    }

    async findByPayload(payload: Payload) {
        const {email} = payload;
        return await this.userModel.findOne({ email });
    }
    
}
