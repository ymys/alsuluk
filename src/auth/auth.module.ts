import { Module, forwardRef } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UserModule } from 'src/user/user.module';
import { JwtStrategy } from './jwt.strategy';
import { GoogleStrategy } from './google.strategy';
import { FacebookStrategy } from "./facebook.strategy";
import { InstagramStrategy } from "./instagram.strategy";

@Module({
  providers: [AuthService, JwtStrategy, GoogleStrategy, FacebookStrategy, InstagramStrategy],
  controllers: [AuthController],
  imports: [forwardRef(() => UserModule)],
  exports: [AuthService]
})
export class AuthModule {}
