import { RolesBuilder } from "nest-access-control";

export enum UserRoles {
    admin = "admin",
    user = "user",
    konsultan = "konsultan"
}

export const roles: RolesBuilder = new RolesBuilder();

roles.grant(UserRoles.user)
    .readAny(["categories"])
    .createAny(["categoriesSelf"])
    .deleteAny(["category"])
    .updateAny(["category"])
    .grant(UserRoles.admin)
    .extend(UserRoles.user)
    .createAny(["categoriesDefault"]);
   
