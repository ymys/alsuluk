import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { Payload } from 'src/auth/entities/payload';
import { sign } from 'jsonwebtoken';
import { UserService } from 'src/user/user.service';

@Injectable()
export class AuthService {
    constructor(
        @Inject(forwardRef(() => UserService))
        private userService: UserService
    ) {}

    async signPayload(payload: Payload) {
        return sign(payload, process.env.SECRET_KEY, { expiresIn: '1d' });
    }

    async validateUser(payload: Payload) {
        return await this.userService.findByPayload(payload);
    }

    googleLogin(req) {
        if (!req.user) {
          return 'No user from google'
        }
        return {
          message: 'User Info from Google',
          user: req.user
        }
    }
}
