import { IsEmail, IsNotEmpty, Validate } from "class-validator";

export class RegisterDTO {
    // @Validate(UniqueValidator)
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    password: string;
}