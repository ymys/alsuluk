import {PassportStrategy} from '@nestjs/passport';
import {Strategy, VerifyCallback} from 'passport-google-oauth20'
import { Injectable } from '@nestjs/common';

@Injectable()

export class InstagramStrategy extends PassportStrategy(Strategy, 'instagram'){

    constructor() {
        super({
            clientID: process.env.INSTAGRAM_ID_APLIKASI,
            clientSecret: process.env.INSTAGRAM_SECRET_APLIKASI,
            callbackURL: `${process.env.APP_URL}/api/v1/auth/instagram/callback`,
            scope: ['email', 'profile']
        })
    }

    async validate(accessToken: string, refreshToken:string, profile: any, done: VerifyCallback) : Promise<any>{
        const {name, emails, photos} = profile

        const user = {
            email: emails[0].value,
            firstName: name.givenName,
            lastName: name.familyName,
            // picture: photos[0].value,
            accessToken
        }

        done(null, user)
    }

}