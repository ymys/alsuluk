import { Body, Controller, Get, Post, UseGuards, Response, Request, Req, HttpStatus } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { ACGuard, UseRoles } from 'nest-access-control';
import { RegisterDTO } from './dto/register.dto';
import { UserService } from 'src/user/user.service';
import { AuthService } from './auth.service';
import { LoginDTO } from './dto/login.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) { }

  @Get("/get-user")
  @UseGuards(AuthGuard("jwt"), ACGuard)
  @UseRoles({
    resource: 'category',
    action: 'read',
    possession: 'any',
  })
  async getUser(@Request() req) {
    return req.user;
  }

  @Post('register')
  async register(@Response() res, @Body() RegisterDTO: RegisterDTO) {
    const token = await this.authService.signPayload({ email: RegisterDTO.email });

    let newUser = { ...RegisterDTO, access_token: token }
    const user = await this.userService.create(newUser);

    res
      .status(201)
      .json({
        'statusCode': 201,
        'message': 'Register successfully',
        'values': user
      });
  }

  @Post('login')
  async login(@Response() res, @Body() UserDTO: LoginDTO) {
    const user = await this.userService.findByLogin(UserDTO);
    res
      .status(200)
      .json({
        'statusCode': 200,
        'message': 'Login successfully',
        'values': user
      });
  }

  @Get('google')
  @UseGuards(AuthGuard('google'))
  async googleAuth(@Req() req) { }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  googleAuthRedirect(@Req() req) {
    return this.authService.googleLogin(req)
  }

  @Get("facebook")
  @UseGuards(AuthGuard("facebook"))
  async facebookLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  // @Get("facebook/callback")
  // @UseGuards(AuthGuard("facebook"))
  // async facebookLoginRedirect(@Req() req: Request): Promise<any> {
  //   return {
  //     statusCode: HttpStatus.OK,
  //     data: req.user,
  //   };
  // }

  @Get("instagram")
  @UseGuards(AuthGuard("instagram"))
  async instagramLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  // @Get("instagram/callback")
  // @UseGuards(AuthGuard("instagram"))
  // async instagramLoginRedirect(@Req() req: Request): Promise<any> {
  //   return {
  //     statusCode: HttpStatus.OK,
  //     data: req.user,
  //   };
  // }
}
