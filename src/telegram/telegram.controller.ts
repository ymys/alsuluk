import { Controller, Get, Res, HttpStatus, Post, Request } from '@nestjs/common';
import { TelegramService } from './telegram.service';

@Controller('telegram')
export class TelegramController {
    constructor(private botService:TelegramService) {}

    @Post(`/bot`)
    async webhook(@Request() res) {
        if(res.body.message) {
           console.log(await this.botService.prosesWebhook(res.body.message))
        } else {
            this.botService.prosesCallbackQuery(res.body.callback_query)
            await this.botService.sendMessage(res.body.callback_query.from.id, 'selesai');
        }
    }
}
