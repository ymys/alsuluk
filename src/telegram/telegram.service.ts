import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MongooseModule } from '@nestjs/mongoose';
import { User } from 'src/user/entities/user';

const TelegramBot = require('node-telegram-bot-api');

@Injectable()
export class TelegramService implements OnModuleInit {
    constructor(
        @InjectModel('User') private userModel: Model<User>,
    ) {}

    onModuleInit() {
        this.init();
    }

    init() {
        const token = process.env.TELEGRAM_TOKEN;
        const bot = new TelegramBot(token);
        bot.setWebHook(`${process.env.APP_URL}/api/v1/telegram/bot`)
        return bot;
    }

    async request(data = {}) {
        return await this.init().sendMessage(data['chat_id'], data['text'], data);
    }

    async sendMessage( chatId, text, messageReplyID = false, forceReply = false, disablePreview = false) {
        const data = {
            chat_id: chatId,
            text: text,
            reply_to_message_id: messageReplyID,
            disable_web_page_preview: disablePreview,
            reply_markup: JSON.stringify({'force_reply' : forceReply})
        }
        return await this.request(data)
    }

    async sendInlineKeyboard(chatID, text, keyboard = [], inline = false) {
        const replyMarkup = {
            'keyboard': keyboard,
            'resize_keyboard': true,
        };

        const data = {
            'chat_id' : chatID,
            'text' : text,
            'parse_mode' : 'Markdown',
        };

        data['reply_markup'] = inline
            ? JSON.stringify({inline_keyboard: keyboard})
            : JSON.stringify(replyMarkup)

        return await this.request(data)
    }

    prosesCallbackQuery(data) {
        const message = data.message
        message.title = message.text,
        message.from = data.from,
        message.text = data.data
        this.prosesWebhook(message);
    }

    async prosesWebhook(data) {
        let chat_id = data['chat']['id'];
        let message_id = data['message_id'];
        let first_name = data['from']['first_name'];

        let teks: string = null;
        if (data['caption']) {
            teks = data['caption'];
        }

        if (data['reply_to_message'] && data['reply_to_message']['text']) {
            teks = data['reply_to_message']['text'];
        } else if (data['text']) {
            teks = data['text'];
        }

        if (teks) {
            let splitText = teks.split(" ");

            switch(splitText[0]) {
                case "/start": {
                    let keyboard = [
                        [
                            { text: 'Binding Data', callback_data: 'Binding' }
                        ],
                        [
                            { text: 'Daftar Kategori', callback_data: 'Daftar Kategori' },
                            { text: 'Daftar Jurnal', callback_data: 'Daftar Jurnal' }
                        ]
                    ]
                    await this.sendInlineKeyboard(chat_id, "Selamat datang di bot kami, silahkan pilih menu yang tersedia", keyboard)
                    break;
                }
                case "Binding": {
                    await this.sendMessage(chat_id, 'Silahkan ketik perintah seperti dibawah ini. \n /binding {email_anda}');
                    break;
                }
                case "/binding": {
                    let findUser = await this.userModel.findOne({ email:  splitText[1] });
                    if(!findUser) {
                        await this.sendMessage(chat_id, `Maaf email tersebut belum terdaftar di aplikasi kami . silahkan melakukan register melalui link dibawah ini : \n${process.env.APP_URL}/api/v1/auth/register`);
                    } else {
                        await this.userModel.findByIdAndUpdate(findUser._id, { telegram_id: chat_id });
                        await this.sendMessage(chat_id, `Akun anda sudah terhubung dengan bot kami . Terimakasih !`);
                    }
                    break;
                }
                default: {
                    break;
                }
            }

            return teks;
        }
    }
}